# Chargement de nos données d'enquete
dt = read.csv("testEnquete.csv", header = TRUE, sep = ",")

# Explorer les données
head(dt)

# Compter les valeurs manquante
colSums(is.na(dt))

# Conserver les variables catégorielles
library(dplyr)
dt_cat = dt %>% select_if(is.character)

head(dt_cat)

# Compte les character vide
colSums(dt_cat == "")

# Remplacer les character vide par NA

dt_cat[dt_cat == ""] = NA

# Compte les NA
colSums(is.na(dt_cat))

# Suppimer les NA
dt_cat = na.omit(dt_cat)

install.packages("FactoMineR", dependencies = TRUE, INSTALL_opts = '--no-lock')
library(FactoMineR)
install.packages("factoextra", dependencies = TRUE, INSTALL_opts = '--no-lock')
library(factoextra)

# Analyse de Correspondances Multiples
res.ca = MCA(dt_cat, graph = TRUE)

summary(res.ca)

# Visualisation des variables
fviz_mca_var(res.ca,
             col.var = "contrib",
             gradient.cols = c("#00AFBB",
                               "#E7B800",
                               "#FC4E07"),
             repel = TRUE)

# Visualisation des individus
fviz_mca_ind(res.ca,
             col.ind = "cos2",
             gradient.cols = c("#00AFBB",
                               "#E7B800",
                               "#FC4E07"),
             repel = TRUE)

# Visualisation des individus avec plotly
fviz_mca_biplot(res.ca,
                col.var = "contrib",
                gradient.cols = c("#00AFBB",
                                  "#E7B800",
                                  "#FC4E07"),
                repel = TRUE)

