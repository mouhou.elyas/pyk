# Charger les données iris
data(iris)

ncol(iris)
nrow(iris)

head(iris)

# plot iris
library(ggplot2)
ggplot(iris, aes(x = Sepal.Length,
                 y = Sepal.Width,
                 color = Species,
                 size=6)) +
    geom_point()

# Pairplot iris
install.packages("GGally")
library(GGally) 

GGally::ggpairs(iris,
                aes(color = Species,
                alpha = 0.4)
                )
# centrer et réduire les données
help(scale)
iris_scaled <- scale(iris[, 1:4])

iris[, c(-3, -5)] # Demonstration du filtrage des colonnes
iris[c(-3, -5),] # Demonstration du filtrage des lignes

# colnames(iris_scaled) <- colnames(iris[, 1:4])
iris_scaled <- as.data.frame(iris_scaled)
iris_scaled$Species <- iris$Species

# Pairplot iris_scaled
GGally::ggpairs(iris_scaled,
                aes(color = Species,
                alpha = 0.4)
                )

# Construction du cluster hierarchique
help(dist)
distances <- dist(iris_scaled[, 1:4])
heatmap(as.matrix(distances))

help(hclust)
mon_cluster <- hclust(distances, "ward.D2")

# Affichage du dendrogramme
install.packages("factoextra")
library(factoextra)
help(fviz_dend)
plot(mon_cluster)

factoextra::fviz_dend(mon_cluster,
                      k = 3,
                      cex = 0.6)

# Comparaison avec les vraies classes
table(iris$Species, cutree(mon_cluster, 3))

cluster_cut <- cutree(mon_cluster, 2)

tapply(iris_scaled[,1], cluster_cut, mean)
tapply(iris_scaled[,2], cluster_cut, mean)
tapply(iris_scaled[,3], cluster_cut, mean)
tapply(iris_scaled[,4], cluster_cut, mean)

# radar plot des moyennes des variables par cluster
install.packages("fmsb")
library(fmsb)
help(radarchart)

iris_scaled$cluster <- cluster_cut
iris_moyennes <- aggregate(iris_scaled[, 1:4],
                           by = list(iris_scaled$cluster),
                           mean)
iris_moyennes
iris_moy <- apply(iris[, -5], 2, mean)
iris_sd <- apply(iris[, -5], 2, sd)

iris_moyennes[,1]<-NULL

# Unscale the data
iris_moyennes <- iris_moyennes * iris_sd + iris_moy
iris_moyennes

# Build the data for the radar plot

radarchart(iris_moyennes,
           axistype = 1,
           seg = 4,
           pcol = c("blue", "red"),
           plwd = 4,
           cglcol = "grey",
           cglty = 1,
           axislabcol = "black",
           caxislabels = seq(0, 1, 0.2),
           cglwd = 0.8,
           vlcex = 0.8
           )

iris_moyennes
