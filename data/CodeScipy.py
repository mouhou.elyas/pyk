import numpy as np, import matplotlib.pyplot as plt, pandas as pd, scipy as sp


### Tester les moyennes
### H0 : Les vins rouges ont des notes inférieur 

### Tester les fréquences
### H0 : Les passagers en 3ème classe ont eu un taux de survie inférieur aux passagers de 1ère classe

### Interpolation
from scipy import interpolate

x = np.arange(0,11)
y = np.exp(-x/3.0)
f = interpolate.interp1d(x, y)
f(9.8)

new_x = np.linspace(0,10,50)
new_y = f(new_x)

plt.scatter(x,y)
plt.scatter(new_x,new_y)
plt.show

#### Trouvez la meilleur méthode d'interpolation pour les points suivant en affichant les différents résultats avec différents label ( pensez listes et boucles ) :

x = np.linspace(0,5,200)
y = 1/9*x**(3.20) - 4/7*x**2 + 124 + np.random.randn(x.shape[0])/2
plt.scatter(x,y)
plt.show()

### Optimisation d'une courbe 

def f(x, a, b, c, d):
    return(a*x**3 + b*x**2 + c*x + d)

from scipy import optimize

param, mcov = optimize.curve_fit(f,x,y)

plt.scatter(x, y)
plt.plot(x, f(x, param[0], param[1], param[2], param[3]), c='r')

plt.show()

### Filtration du signal
from scipy import signal

x = np.linspace(0,10,300)
y = x/8 + np.sin(1/2*x)/4 + np.cos(2*x+4)/3 + np.sin(4*x)/2 + np.random.randn(x.shape[0])/10

new_y=signal.detrend(y)

plt.plot(x,y)
plt.plot(x,new_y)
plt.show()

### Analyse des fréquences par tansformée de Fourrier
from scipy import fft
fourrier = fft.fft(new_y)
power = np.abs(fourrier)
frequences = fft.fftfreq(new_y.size)
plt.plot(np.abs(frequences), power)
plt.show()

fourrier[power<5]=0
plt.plot(np.abs(frequences), np.abs(fourrier))
plt.show()

invFourrier = fft.ifft(fourrier)
plt.plot(x,new_y)
plt.plot(x,invFourrier)
