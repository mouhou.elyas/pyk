# Chargement des packages nécessaires
library(caret)
library(randomForest)

# Préparer les données (utilisation de mtcars comme exemple)
data(mtcars)
mtcars$am <- as.factor(mtcars$am)  # Convertir la variable cible en facteur

# Définir le contrôle de la validation croisée
train_control <- trainControl(method = "cv", number = 10)  # Validation croisée 10-fold

# Définir la grille de recherche des hyperparamètres
tune_grid <- expand.grid(
  mtry = 2:5,  # Nombre de variables aléatoires 
  # sélectionnées à chaque scission
)

# Ajuster le modèle de forêt aléatoire avec la validation croisée
model_rf_cv <- train(
  am ~ ., 
  data = mtcars, 
  method = "rf",  # Utilisation de randomForest
  trControl = train_control,
  tuneGrid = tune_grid,
  metric = "Accuracy"  # Métrique d'évaluation
)

# Afficher les résultats
print(model_rf_cv)
