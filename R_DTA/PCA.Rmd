---
title: "ACP"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
df = mtcars
```

```{r}
library("FactoMineR")
res.pca <- FactoMineR::PCA(df, scale.unit = TRUE, graph = FALSE)
```

```{r}
print(res.pca)
```

```{r}
library(factoextra)
```

```{r}
factoextra::get_eigenvalue(res.pca) # Extract the eigenvalues/variances of principal components
```

```{r}
fviz_eig(res.pca) # Visualize the eigenvalues
```

```{r}
get_pca_ind(res.pca)
get_pca_var(res.pca) # Extract the results for individuals and variables, respectively.
```

```{r}
fviz_pca_ind(res.pca)
fviz_pca_var(res.pca) # Visualize the results individuals and variables, respectively.
```

```{r}
fviz_pca_biplot(res.pca) # Make a biplot of individuals and variables.
```

```{r}
df1 <- housetasks
```


```{r}
res.ca <- CA(df1)
```


```{r}
factoextra::get_eigenvalue(res.ca) # Extract the eigenvalues/variances of principal components
```

```{r}
fviz_eig(res.ca) # Visualize the eigenvalues
```

```{r}
get_ca_row(res.ca)
get_ca_col(res.ca) # Extract the results for rows and columns, respectively.
```

```{r}
fviz_ca_row(res.ca)
fviz_ca_col(res.ca) # Visualize the results for rows and columns, respectively.
```

```{r}
fviz_ca_biplot(res.ca) # Make a biplot of rows and columns.
```

```{r}
library("corrplot")
row <- get_ca_row(res.ca)
corrplot(row$contrib, is.corr=FALSE)    
```

```{r}
# Contributions of rows to dimension 1
fviz_contrib(res.ca, choice = "row", axes = 1, top = 10)
# Contributions of rows to dimension 2
fviz_contrib(res.ca, choice = "row", axes = 2, top = 10)
# Contributions of rows to dimension 3
fviz_contrib(res.ca, choice = "row", axes = 3, top = 10)
```



